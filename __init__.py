r"""
.. module:: MTIpython.mtiprint
    :platform: Unix, Windows
    :synopsis: packages for human readable output, either in Latex, HTML of plain text. Works in conjunction with Jupyter

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
